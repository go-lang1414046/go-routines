package goroutines

import (
	"fmt"
	"testing"
	"time"
)

func TestCreateChannel(t *testing.T) {
	channel := make(chan string)

	channel <- "Nurdin"
	data := <-channel

	fmt.Println("Data", data)

	fmt.Println("channel", <-channel)

	defer close(channel) // bisa pake ini untuk ignore done or error

	close(channel) // bisa pake ini
}

func TestCreateChannel2(t *testing.T) {
	channel := make(chan string)
	defer close(channel) // bisa pake ini untuk ignore done or error
	go func() {
		time.Sleep(2 * time.Second)
		//channel <- "Nurdin rolissalim"
		fmt.Println("Selesai mengirim Data ke channel")
	}()
	data := <-channel
	fmt.Println("Data", data)
	time.Sleep(5 * time.Second)
}
