package goroutines

import (
	"fmt"
	"testing"
	"time"
)

func RunHello() {
	fmt.Println("Hello")
}

func TestCreateGoroutine(t *testing.T) {
	go RunHello()
	fmt.Println("Ups")
	time.Sleep(2 * time.Second)
}

func DisplayNumber(number int) {
	fmt.Println("Display", number)
}

func TestManyGoRoutine(t *testing.T) {
	for i := 1; i <= 100000; i++ {
		go DisplayNumber(i)
	}

	time.Sleep(7 * time.Second)
}
